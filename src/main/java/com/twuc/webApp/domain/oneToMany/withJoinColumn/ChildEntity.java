package com.twuc.webApp.domain.oneToMany.withJoinColumn;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Objects;

// TODO
//
// ChildEntity 应当具备一个自动生成的 id 以及一个字符串 name。请实现 ChildEntity。ChildEntity 的
// 数据表的参考定义如下：
//
// +───────────────────+──────────────+──────────────────────────────+
// | Column            | Type         | Additional                   |
// +───────────────────+──────────────+──────────────────────────────+
// | id                | bigint       | primary key, auto_increment  |
// | name              | varchar(20)  | not null                     |
// | parent_entity_id  | bigint       | null                         |
// +───────────────────+──────────────+──────────────────────────────+
//
// <--start-
@Entity
public class ChildEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @ManyToOne
    private ParentEntity parentEntity;

    public ChildEntity() {
    }

    public ChildEntity(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ParentEntity getParentEntity() {
        return parentEntity;
    }

    public void setParentEntity(ParentEntity parentEntity) {
        this.parentEntity = parentEntity;
    }
}
// --end->
